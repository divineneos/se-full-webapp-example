// bookRoutes.js
var express = require('express');
var app = express();
var bookRoutes = express.Router();

// Require Book model in our routes module
var Book = require('../models/Book');

// Defined store route
bookRoutes.route('/').post(function (req, res) {
  var book = new Book(req.body);
  book.save()
    .then(function(book) {
      res.status(200).json({success:true, 'message': 'Book added successfully'});
    })
    .catch(function(err) {
      res.status(400).json({success:false, message:"Error saving to database!"});
    });
});

// Defined get data(index or listing) route
bookRoutes.route('/').get(function (req, res) {
  Book.find(function (err, books) {
    if(err) {
      res.json({success:false, 'message': 'Could not fetch books from database!'});
    }
    else {
      res.status(200).json({success:true, 'message': 'Books fetched successfully', data: books});
    }
  });
});

// Defined getting a single book route
bookRoutes.route('/:id').get(function (req, res) {
  var id = req.params.id;
  Book.findById(id, function (err, book) {
    res.json({success:true, 'message': 'Book fetched successfully', data: book});
  });
});

// Defined update route
bookRoutes.route('/:id').patch(function (req, res) {
  Book.findById(req.params.id, function(err, book) {
    if (!book)
      res.json({success:false, 'message': 'Book not found!'});
    else {
      book.title = req.body.title;
      book.price = req.body.price;
      book.save().then(function(book) {
        res.json({success:true, 'message': 'Book updated successfully', data: book});
      })
      .catch(function(err) {
        res.json({success:false, 'message': 'Could not update book'});
      });
    }
  });
});

// Defined delete | remove | destroy route
bookRoutes.route('/:id').delete(function (req, res) {
  Book.findByIdAndRemove({_id: req.params.id}, function(err, book){
		if(err) res.json({success:false, 'message': 'Could not remove book'});
		else res.json({success:true, 'message': 'Book successfully removed'});
	});
});
module.exports = bookRoutes;