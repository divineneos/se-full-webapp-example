var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Define collection and schema for Books
var Book = new Schema({
  title: {
    type: String
  },
  author: {
    type: String
  },
  rating: {
    type: Number
  },
  price: {
    type: Number
  }
},{
	collection: 'books'
});

module.exports = mongoose.model('Book', Book);