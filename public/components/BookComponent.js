var Book = {
    props: ['id'],
	template: `
        <div class="book-item">
            <div class="book-title">
                <span class="book-title-text">{{bookData.title}} ({{bookData.rating}})</span>
            </div>
            <div class="book-author">{{bookData.author}}</div>
            <div class="book-price">Price: {{bookData.price}}$</div>
        </div>
    `,
    data() {
        return {
            bookData: {}
        };
    },
    methods: {
        fetchBookData() {
            fetch(globalApiRoute + '/books/' + this.id)
            .then(response => response.json())
            .then(response => {            
                this.bookData = response.data;
            });
        }
    },
    created() {
        this.fetchBookData();
    }
};
Vue.component('book', Book);