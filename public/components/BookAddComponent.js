var BookAdd = {
	template: `
        <div class="book-form">
            <label for="bookTitle">Title:</label>
            <input type="text" name="bookTitle" class="book-form-input" v-model="bookTitle"/><br/>

            <label for="bookAuthor">Author:</label>
            <input type="text" name="bookAuthor" class="book-form-input" v-model="bookAuthor"/><br/>

            <label for="bookRating">Rating:</label>
            <input type="text" name="bookRating" class="book-form-input" v-model="bookRating"/><br/>

            <label for="bookPrice">Price:</label>
            <input type="text" name="bookPrice" class="book-form-input" v-model="bookPrice"/><br/>

            <button @click="addNewBook">Add book</button>
        </div>
    `,
    data() {
        return {
            bookTitle : '',
            bookAuthor : '',
            bookRating : '',
            bookPrice : ''
        };
    },
    methods: {
        addNewBook() {
            this.$http.post(globalApiRoute + '/books/', {
                title: this.bookTitle,
                author: this.bookAuthor,
                rating: this.bookRating,
                price: this.bookPrice
            })
            .then(response => response.json())
            .then(response => {
                if(response.success) {
                    router.push({ name: 'BookList'});
                    alert("Uspjesno dodata knjiga!");
                }
                else {
                    alert("Greska pri dodavanju knjige!");
                }
            });
        }
    }
};
Vue.component('bookadd', BookAdd);