var BookList = {
    template: `
        <div class="book-list">
            <div class="book-list-item" v-for="tmpBook in allBooksData">
                <h3 class="book-list-item-title" @click="viewBook(tmpBook._id)">{{ tmpBook.title }}</h3>
                <span class="book-item-delete" @click="deleteBook(tmpBook._id)">  X</span>
            </div>
        </div>
    `,
    data() {
        return {
            allBooksData: []
        };
    },
    methods: {
        fetchAllBooksData() {
            fetch(globalApiRoute + '/books/')
            .then(response => response.json())
            .then(response => {            
                this.allBooksData = response.data;
            });
        },
        viewBook(id) {
            router.push({ name: 'Book', params: { id: id }});
        },
        deleteBook(id) {
            this.$http.delete(globalApiRoute + '/books/' + id)
            .then((response) => {
                let deleteData = response.data;
                if(deleteData.success) {
                    this.deleteBookFromArray(id);
                    alert('Uspjesno obrisana knjiga!');
                }
                else {
                    alert('Neuspjesno obrisana knjiga!');
                }
            });
        },
        deleteBookFromArray(id) {
            for(var i = 0; i < this.allBooksData.length; i++) {
                if(this.allBooksData[i]._id == id) {
                    this.allBooksData.splice(i, 1);
                }
            }
        }
    },
    created() {
        this.fetchAllBooksData();
    }
};
Vue.component('booklist', BookList);