var globalApiRoute = 'http://localhost:4000';

var router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/about', name: 'About', component: About },
        { path: '/book', name: 'BookList', component: BookList },
        { path: '/book/:id', name: 'Book', component: Book, props: true },
        { path: '/addBook', name: 'BookAdd', component: BookAdd },
        { path: '/', name: 'Home', component: Home }
    ]
});

new Vue({
    el: "#root",
    router: router
});