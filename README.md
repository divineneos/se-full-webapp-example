Uvodni koraci:
- Instalacija MongoDB, NodeJS i Robo 3T klijenta
- Uvodna prica o ovim tehnologijama
- Kreiranje baze se-full-webapp-example u MongoDB kroz Robo 3T

Koraci:
1 Kreiranje stabla direktorijuma
-- app
-- config
---- db.js
-- public

Kreiranje package.json fajla komandom: npm init
Instalacija modula: express body-parser cors mongoose nodemon
Ukljucivanje preuzetih modula u glavni NodeJS fajl, kao i konfiguracionog fajla za bazu.

2 Podesavanje konekcije na bazu. Pravljenje jednostavnog express servera, osnovne rute na kojoj se stampa "Hello world!" i testiranje da li radi, uz odabir porta (npr. 4000).

3 Formiranje Book modela i njegovo ukljucivanje

4 Formiranje CRUD ruta za Book model. Praviti prvo jednu po jednu u glavni fajl i pokazati razlicite metode: GET, POST, PATCH, DELETE. Zatim izdvojiti sve u poseban fajl.