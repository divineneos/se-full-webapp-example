var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');

var config = require('./config/DB');

/* Connect to MongoDB and create server instance */
mongoose.connect(config.DB); // Konekcija na bazu
var port = 4000;
var app = express(); // Jedna express instanca, jedan server

/* Models */
var Book = require('./app/models/Book');

/* Middleware */
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cors());

/* Routes */
var bookRoutes = require('./app/routes/bookRoutes');
app.use('/books', bookRoutes);

app.get('*', function(req, res){
	res.sendfile(__dirname + '/public/index.html');
});
// app.get('/*', function(req, res){
// 	res.sendfile('./public/index.html');
// });

app.listen(port, function(){ // Pokretanje servera na port 4000
	console.log("Server started!");
});